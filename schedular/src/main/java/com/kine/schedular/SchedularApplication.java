package com.kine.schedular;

import org.jobrunr.scheduling.JobScheduler;
import org.jobrunr.scheduling.cron.Cron;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import com.kine.schedular.service.ISchedularService;

@SpringBootApplication
public class SchedularApplication {

	@Autowired
	private JobScheduler jobScheduler;
	
	@Autowired
	private ISchedularService schedularService;
	
	public static void main(String[] args) {
		SpringApplication.run(SchedularApplication.class, args);
	}
	
	@Bean
	public void run()
	{
		jobScheduler.scheduleRecurrently(() -> schedularService.executeBackgroundJob(), Cron.minutely());
	}
}