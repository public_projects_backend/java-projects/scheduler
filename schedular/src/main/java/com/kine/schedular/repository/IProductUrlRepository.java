package com.kine.schedular.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.stereotype.Repository;

import com.kine.schedular.model.ProductUrls;

@Repository
public interface IProductUrlRepository extends JpaRepository<ProductUrls, Integer> {
	
	List<ProductUrls> findAllByTriggerTimeBetween(@Temporal(TemporalType.DATE) Date date, Date date1);
}
