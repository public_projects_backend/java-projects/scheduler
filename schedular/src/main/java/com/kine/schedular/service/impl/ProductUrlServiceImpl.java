package com.kine.schedular.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.kine.schedular.model.ProductUrls;
import com.kine.schedular.repository.IProductUrlRepository;
import com.kine.schedular.service.IProductUrlService;

@Service
public class ProductUrlServiceImpl implements IProductUrlService{
	@Autowired
	private IProductUrlRepository productUrlRepository; 
	
	@Override
	public void fetchProductUrls(){
		Calendar date = Calendar.getInstance();
		long timeInSecs = date.getTimeInMillis();
		List<ProductUrls> productUrls = productUrlRepository.findAllByTriggerTimeBetween(new Date(), new Date(timeInSecs + (1 * 60 * 1000)));
		for (ProductUrls productUrls2 : productUrls) {
			System.out.println(productUrls2.getProductUrl());
		}
	}
}
