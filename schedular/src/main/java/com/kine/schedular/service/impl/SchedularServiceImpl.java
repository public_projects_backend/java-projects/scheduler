package com.kine.schedular.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.kine.schedular.service.IProductUrlService;
import com.kine.schedular.service.ISchedularService;

@Service
public class SchedularServiceImpl implements ISchedularService
{

	@Autowired
	private IProductUrlService productUrlService;
	@Override
	public void executeBackgroundJob() {
		productUrlService.fetchProductUrls();
	}
	
}
